.SILENT :
.PHONY : test

test:
	docker build -t registry.gitlab.com/thoralf.liersch/docker-basebox:bats .
	bats test
	docker stop $$(docker ps -a -q --filter "label=bats-type")
	docker rm $$(docker ps -a -q --filter "label=bats-type")
	docker rmi registry.gitlab.com/thoralf.liersch/docker-basebox:bats

build:
	docker build -t registry.gitlab.com/thoralf.liersch/docker-basebox:latest .

init:
	if [ ! -f config.env ]; then cp config.env.dist config.env; fi

run:
	docker run --detach --name basebox registry.gitlab.com/thoralf.liersch/docker-basebox

stop:
	docker stop basebox

rm:
	docker rm basebox

clean:
	docker rmi registry.gitlab.com/thoralf.liersch/docker-basebox

exec:
	docker exec -i -t basebox /bin/bash
