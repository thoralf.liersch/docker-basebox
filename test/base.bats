@test "version is correct" {
  run docker run --label bats-type="alpine_version" registry.gitlab.com/thoralf.liersch/docker-basebox:bats cat /etc/os-release
  [ $status -eq 0 ]
  [ "${lines[2]}" = "VERSION_ID=3.5.0" ]
}

@test "check timezone" {
    run docker run --label bats-type="check_timezone" registry.gitlab.com/thoralf.liersch/docker-basebox:bats date +%Z
    [ $status -eq 0 ]
    [ "$output" = "UTC" ]
}

@test "root password is disabled" {
  run docker run --label bats-type="check_disabled_rootpassword" --user nobody registry.gitlab.com/thoralf.liersch/docker-basebox:bats su
  [ $status -eq 1 ]
}

@test "cache is empty" {
    run docker run --label bats-type="check_emptycache" registry.gitlab.com/thoralf.liersch/docker-basebox:bats sh -c "ls -1 /var/cache/apk | wc -l"
    [ $status -eq 0 ]
    [ "$output" = "0" ]
}

@test "repository list is correct" {
  run docker run --label bats-type="check_repositorylist" registry.gitlab.com/thoralf.liersch/docker-basebox:bats cat /etc/apk/repositories
  [ $status -eq 0 ]
  [ "${lines[0]}" = "http://dl-cdn.alpinelinux.org/alpine/v3.5/main" ]
  [ "${lines[1]}" = "http://dl-cdn.alpinelinux.org/alpine/v3.5/community" ]
  [ "${lines[2]}" = "" ]
}
